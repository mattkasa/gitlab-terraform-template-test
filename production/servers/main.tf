resource "local_file" "foo" {
  filename = "${path.module}/servers.bar"
  content = "bar!"
  directory_permission = "0755"
  file_permission = "0755"
}
