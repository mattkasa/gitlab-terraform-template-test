resource "local_file" "foo" {
  filename = "${path.module}/dns.bar"
  content = "bar!"
  directory_permission = "0755"
  file_permission = "0755"
}
